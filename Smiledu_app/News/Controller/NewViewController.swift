//
//  NewViewController.swift
//  Smiledu_app
//
//  Created by Anthony Montes Larios on 20/02/18.
//  Copyright © 2018 Anthony Montes Larios. All rights reserved.
//

import UIKit

class NewViewController: UIViewController {

    var NewArray = [String:AnyObject]()
    
    @IBOutlet weak var newDetailImage: UIImageView!
    
    @IBOutlet weak var newDetailTitle: UILabel!
    
    @IBOutlet weak var newDetailDate: UILabel!
    @IBOutlet weak var newDetailSubTitle: UILabel!
    @IBOutlet weak var newDetailSubDate: UILabel!
    @IBOutlet weak var newDetailTextArticle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newDetailTitle.text = NewArray["titulo"] as? String
        
        let newImagen:[String] = NewArray["foto_noticia"] as!  [String]
        
        newDetailImage.sd_setImage(with: URL(string: String(describing: newImagen[0])), placeholderImage: UIImage(named: "smiledu_smiledustore"))
        
        newDetailSubTitle.text = NewArray["titulo"] as? String
        
        let bodytext = NewArray["cuerpo"] as! String
        
        newDetailTextArticle.numberOfLines = 0
        newDetailTextArticle.lineBreakMode = .byWordWrapping
        
        let attrStr = NSAttributedString(string: bodytext)
        
        
    
        //newDetailTextArticle.text = attrStr
        
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


