//
//  LoginInputs.swift
//  Smiledu_app
//
//  Created by Giovanni Camana on 2/20/18.
//  Copyright © 2018 Anthony Montes Larios. All rights reserved.
//
import UIKit

class UserInputs: UITextField, UITextFieldDelegate {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        delegate = self
        createBorder()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        delegate = self

        self.layer.backgroundColor = UIColor.clear.cgColor
//        self.attributedPlaceholder = NSAttributedString(string: "placeholder text", attributes: [NSAttributedStringKey.foregroundColor : UIColor.yellow.cgColor])
        self.layer.cornerRadius = self.frame.size.height/2
        
        self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.frame.height))
        
        self.leftViewMode =  UITextFieldViewMode.always
        
        self.borderStyle = UITextBorderStyle.none
        self.font = UIFont.systemFont(ofSize: 12)
        self.keyboardType = UIKeyboardType.default
        self.returnKeyType = UIReturnKeyType.done

        self.clearButtonMode = UITextFieldViewMode.whileEditing
        self.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        self.clipsToBounds = true
        
        createBorder()
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 20, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 20, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }

    
    func createBorder()  {
        let border = CALayer()
        let width = CGFloat(2.0)
        self.layer.borderColor =  UIColor.white.cgColor
        border.borderWidth = width
        self.layer.cornerRadius = 20.0
        self.layer.borderWidth = 1.0
        self.frame.size.height = 39.0
        self.layer.addSublayer(border)
        
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("lost focused")
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("focused")
    }
}

