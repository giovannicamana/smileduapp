//
//  NoticiasViewController.swift
//  Smiledu_app
//
//  Created by Anthony Montes Larios on 14/02/18.
//  Copyright © 2018 Anthony Montes Larios. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class NewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var newsDictionary = [String:AnyObject]()
    var newArray = [AnyObject]()
    
    @IBOutlet weak var newsSearchBar: UISearchBar!
    @IBOutlet weak var newsTableView: UITableView!
    override func viewDidLoad() {
        
        Alamofire.request(globalVariables.newsAPI).responseJSON { response in
            if let json = response.result.value {
                self.newsDictionary = json as! [String : AnyObject]
                
                self.newArray = self.newsDictionary["response"] as! [AnyObject]
                
                print("Imprimiendo JSON \(self.newArray)")
                
                print("contando \(self.newArray.count)")
                
                self.newsTableView.reloadData()
            }
        }
        
        
        
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellNewsList", for: indexPath) as! newsTableViewCell
        
        //Texto Titulo
        cell.newsListTitle.text = newArray[indexPath.row]["titulo"] as! String
        //Fecha --
        let newDate = newArray[indexPath.row]["fecha_publicacion"] as! String
        //Imagen
        let newImagen:[String] = newArray[indexPath.row]["foto_noticia"] as!  [String]
        
        print(type(of:  newArray[indexPath.row]["foto_noticia"]))
        
        print(type(of: newImagen))
        
        cell.newsListImage.sd_setImage(with: URL(string: String(describing: newImagen[0])), placeholderImage: UIImage(named: "smiledu_smiledustore"))
        
        cell.newsListImage.layer.cornerRadius = 13
        cell.newsListImage.clipsToBounds = true
        cell.newsListImage.animationDuration = 10
        
        cell.newsListImage.autoresizesSubviews = true
        cell.newsListImage.startAnimating()
        
        
        
        cell.newsListImageFilter.layer.cornerRadius = 13
        cell.newsListImageFilter.clipsToBounds = true
        cell.newsListImageFilter.animationDuration = 10
        
        
        cell.newsListImage.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        cell.newsListImage.layer.shadowColor = UIColor.black.cgColor
        cell.newsListImage.layer.shadowOpacity = 1
        cell.newsListImage.layer.shadowOffset = CGSize.zero
        cell.newsListImage.layer.shadowRadius = 10
        
        //cell.newsListImage.layer.borderWidth = 3
        
        
        print("Foto Notica 1 \(newArray[indexPath.row]["foto_noticia"])")
        
        print("Foto Notica 2 \(newImagen)")
        
        
        
        return cell
    }
    
    

     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "DetailNew" {
            if let indexPath = self.newsTableView.indexPathForSelectedRow {
                let NewSelected = newArray[indexPath.row]
                print("Seleccion de noticia \(NewSelected)")
                print(type(of: NewSelected))
                let objDestino = segue.destination as! NewViewController
                objDestino.NewArray = NewSelected as! [String : AnyObject]
            }
        }
        
        
     }
    
}

